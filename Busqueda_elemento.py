import unittest
from selenium import webdriver
from time import sleep


class loginCorreo(unittest.TestCase):

    def setUp(self):
        global driver
        driver = webdriver.Firefox(executable_path='/Users/Tatay/Browsers/geckodriver')
        driver.get('http://www.google.com.co')
        sleep(2)

    def testingresoEmail(self):
        email = "automationwebqaco@gmail.com"
        btn_start_session = driver.find_element_by_id('gb_70')
        assert btn_start_session.text == "Iniciar sesión"
        print("se encontro el elemento")
        btn_start_session.click()
        sleep(2)
        box_email = driver.find_element_by_id("identifierId")
        box_email.click()
        # box_email.send_keys(email)
        # print("Se ingreso el correo {}.".format(email))
        sleep(2)
        btn_next = driver.find_element_by_class_name("RveJvd.snByac")
        assert btn_next.text == "Siguiente"
        btn_next.click()
        sleep(2)
        alerta = driver.find_element_by_class_name("o6cuMc")
        assert alerta.text == "Ingresa un correo electrónico o número de teléfono"
        print("Se ve el texto {} ".format(alerta.text))

    def tearDown(self):
        driver.close()


unittest.main()

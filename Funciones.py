"""Funcion:
Es una unidad de codigo que se puede reutilizar en un programa.
Estructura de una funcion:
def nombreFuncion():'Esta es la sintaxis'
    Se debe dejar una identacion de 4 espacios.

Se debe dejar un salto de linea entre funciones y en la ultima funcion para poder dar inicio al llamado de la o las mismas

nombreFuncion()  'Esta es la sintaxis para llamar y ejecutar esa funcion'

EJEMPLO

def setUp():
    browser = webdriver.Chrome()
    browser.get()

setUp()
"""

from selenium import webdriver
from time import sleep

email = "automationwebqaco@gmail.com"
#Ingreso al navegador
driver = webdriver.Firefox(executable_path='/Users/Browsers/geckodriver')

def levantoNavegador():
    driver.get('http://www.google.com.co')
    sleep(2)

# Ingreso a login de gmail.
def ingresoEmail():
    btn_start_session = driver.find_element_by_id('gb_70')
    assert btn_start_session.text == "Iniciar sesión"
    print("se encontro el elemento")
    btn_start_session.click()
    sleep(2)
    box_email = driver.find_element_by_id("identifierId")
    box_email.click()
    box_email.send_keys(email)
    print("Se ingreso el correo {}.".format(email))
    sleep(2)

# Ingreso del correo
def ingresoCorreo():
    btn_next = driver.find_element_by_class_name("RveJvd.snByac")
    assert btn_next.text == "Siguiente"
    print("Se encontro el boton se siguiente")
    btn_next.click()
    sleep(2)

# Ingreso de la clave
def ingresoClave():
    box_password = driver.find_element_by_name("password")
    if box_password is not None:
        print("Se encontro el campo de contraseña")
    sleep(5)
    box_password.click()
    box_password.send_keys('Web12345Auto')
    sleep(2)
    show_password_close = driver.find_element_by_class_name('wRNPwe.pVlEsd')
    show_password_close.click()
    sleep(2)
    show_password_open = driver.find_element_by_class_name('wRNPwe.S7pdP')
    show_password_open.click()
    sleep(2)
    btn_nextTwo = driver.find_element_by_class_name("RveJvd.snByac")
    assert btn_nextTwo.text == "Siguiente"
    print("Se encontro el boton de siguiente")
    btn_nextTwo.click()
    sleep(2)

# Validar el ingreso
def validarIngreso():
    btn_avatar = driver.find_element_by_class_name('gb_Ia.gbii')
    if btn_avatar is not None:
        print("Se encontro el logo de avatar")
    btn_avatar.click()
    sleep(2)

    login_name_true = driver.find_element_by_class_name('gb_rb.gb_sb')
    if login_name_true.text == "Automation Test":
        print("Es el nombre correcto")

    login_email_true = driver.find_element_by_class_name('gb_tb')
    assert login_email_true.text == "automationwebqaco@gmail.com"
    print("Es el email correcto")
    sleep(2)

# Hacer logout
def logout():
    btn_logout = driver.find_element_by_id('gb_71')
    if btn_logout.text == "Salir":
        print("Se salio del correo y hemos terminado la prueba.")
        btn_logout.click()

levantoNavegador()
ingresoEmail()
ingresoCorreo()
ingresoClave()
validarIngreso()
logout()

# driver.close()





"""email: AutomationWebQAco@gmail.com  clave: Web12345Auto"""


import configparser
import unittest
from selenium import webdriver
from time import sleep

from Page.PageGeneralSettings import PageGeneralSettings
from Page.PageLogIn import PageLogIn


class GeneralConfiguration(unittest.TestCase):

    def setUp(self):
        self.config = configparser.ConfigParser()
        self.config = configparser.ConfigParser()
        self.config.read('keys.ini')
        self.driver = webdriver.Chrome(executable_path=self.config['EXECUTABLE_PATHS']['MonicaChrome'])
        self.driver.get(self.config['URLS']['InitURL'])
        self.driver.maximize_window()
        sleep(2)

    def testChangeSettings(self):
        self.page_LogIn = PageLogIn(self.driver)
        self.page_generalSettings = PageGeneralSettings(self.driver)
        self.page_LogIn.intogmail()
        self.page_generalSettings.goToSettings()
        self.page_generalSettings.language('English (UK)')
        self.page_generalSettings.TelephoneNumberCodeCountry('Antigua y Barbuda')
        self.page_generalSettings.ConversationsForPage('100')
        self.page_generalSettings.ShippingCancellationPeriod('10')
        #self.driver.execute_script("window.scrollTo(0,15)")
        self.page_generalSettings.DefaultResponseForm('Responder a todos')
        self.page_generalSettings.ActionsOfPlacingTheCursorOnAnElement('Habilitar acciones de colocar el cursor sobre un elemento')
        self.page_generalSettings.SendAndArchiveButton('timesnewroman')

    def tearDown(self):
        pass
        #self.driver.close()


unittest.main()
import unittest
import configparser
from time import sleep
from selenium import webdriver
from Page.PageInicial import PageInicial
from Page.PageLogIn import PageLogIn
from Page.Pageinbox import Pageinbox
from Page.PageImap import PageImap
from Page.PageGeneralSettings import PageGeneralSettings
from Page.PageAccounts import PageAccounts
from Page.PageSettingsFilters import PageSettingsFilters
from selenium.webdriver.support import expected_conditions


class loginCorreo(unittest.TestCase):

    def setUp(self):
        self.config = configparser.ConfigParser()
        self.config.read('keys.ini')
        self.driver = webdriver.Chrome(executable_path=self.config['EXECUTABLE_PATHS']['JorgeUChrome'])
        self.driver.get(self.config['URLS']['InitURL'])
        self.page_inicial = PageInicial(self.driver)
        self.page_signup = PageLogIn(self.driver)
        self.page_inbox = Pageinbox(self.driver)
        self.page_imap = PageImap(self.driver)
        self.driver.implicitly_wait(5)
        self.page_account = PageAccounts(self.driver)
        self.page_general_config = PageGeneralSettings(self.driver)
        sleep(2)

        self.driver.implicitly_wait(5)

    def test1IngresoEmail(self):
        self.page_inicial.ingreso_dato("automationwebqaco@gmail.com")
        self.page_inicial.btn_next()
        self.page_inicial.ingreso_password("Web12345Auto")
        self.page_inicial.ver_password() # Isabel
        btn_nextTwo = self.driver.find_element_by_class_name("RveJvd.snByac")  #Diosymar
        assert btn_nextTwo.text == "Siguiente"
        print("Se encontro el boton de siguiente")
        btn_nextTwo.click()
        self.driver.implicitly_wait(5)
        self.page_inicial.encontrar_avatar()
        self.page_signup.validatename()
        self.page_inicial.cerrar_sesion()
    
    def test2gmail(self):
        self.page_signup.intogmail()
        self.page_inbox.btn_write_new_email()
        self.page_inbox.btninboxconfig()
        self.page_inbox.btn_menu()

    def test3Imap(self):
        self.page_imap = PageImap(self.driver)
        self.page_signup.intogmail()
        #self.page_imap.ingresoGmail()
        self.page_imap.ingresoConfiguraciones()
        self.page_imap.ingresoImap()
        self.page_imap.reenvio()
        self.page_imap.descarga()
        self.page_imap.acceso()

    def test4Filter(self):
        self.page_settingsFilter = PageSettingsFilters(self.driver)
        self.page_settingsFilter.goFilter()
        self.page_settingsFilter.createFilter()
        self.page_settingsFilter.removeFilter()

    def test5Account(self):
        self.driver.maximize_window()
        self.page_signup.intogmail()
        self.page_inbox.btninboxconfig()
        self.page_inbox.openGeneralSettingsPage()
        self.page_general_config.selectSettingOption('Cuentas e importación')
        self.page_account.open_other_mail()
        self.page_account.add_email_address()
        self.page_account.change_radio_buttons_values()
        self.page_account.open_buy_storage_link()

    def tearDown(self):
        self.driver.close()


unittest.main()


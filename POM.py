"""
POM Page Object Model, es un patron de diseño. Que se usa actualmente para optimizar la creacion de tus pruebas,
este modelo ofrece las siguientes ventajas.

- El codigo sera mas facil de enteder.
- Es facil de mantener por cualquier persona del equipo.
- Codigo reusable - Aca se puede usar el mismo es script en varias pruebas.

Este modelo necesita dos folder

/Paginas
__init__.py  Python por medio de esto valida que es un paquete y que puede ser importado.
"Este se puede convertir en un constructor"

/pruebas

Cuando estamos usando solo la consola de python el IDLE es importante configurar las variables de entorno para poder
ejecutar los programas adecuadamente que hay en cada folder.


https://www.tutorialselenium.com/2019/02/05/page-object-model-selenium-webdriver/

element = WebDriverWait(driver, 20).until(
 EC.presence_of_element_located((By.ID, "myElement")))

"""
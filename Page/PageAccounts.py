from selenium.webdriver.support.ui import WebDriverWait
from Page.PageInicial import PageInicial
import configparser
from selenium.webdriver.common.action_chains import ActionChains
from time import sleep


class PageAccounts:

    def __init__(self, test):
        self.driver = test
        self.config = configparser.ConfigParser()
        self.config.read('keys.ini')
        self.action = ActionChains(self.driver)
        self.wait = WebDriverWait(self.driver, 10)

    def open_other_mail(self):
        account_logo = self.driver.find_element_by_class_name("gb_Ia.gbii")
        account_logo.click()
        sleep(1)
        self.driver.implicitly_wait(5)
        btn_add_account = self.driver.find_element_by_class_name("gb_Hb")
        btn_add_account.click()
        sleep(2)
        self.driver.implicitly_wait(5)
        current_tab = self.driver.current_window_handle
        self.window_handler()
        self.into_gmail(self.config['TEST_DATA']['secondary_email'], self.config['TEST_DATA']['password_email'], current_tab)
        sleep(2)

    def add_email_address(self):
        link_add_email = self.driver.find_element_by_xpath("//span[contains(text(),'Agregar otra dirección de correo electrónico')]")
        current_tab = self.driver.current_window_handle
        link_add_email.click()
        self.driver.implicitly_wait(5)
        self.window_handler()
        sleep(1)
        tbx_name = self.driver.find_element_by_id("cfn")
        tbx_name.clear()
        tbx_name.send_keys(self.config['TEST_DATA']['add_email_name'])
        self.driver.implicitly_wait(5)
        sleep(1)
        tbx_email = self.driver.find_element_by_id("focus")
        tbx_email.send_keys(self.config['TEST_DATA']['secondary_email'])
        self.driver.implicitly_wait(5)
        sleep(1)
        btn_next = self.driver.find_element_by_id("bttn_sub")
        btn_next.click()
        self.driver.implicitly_wait(5)
        sleep(1)
        btn_send = self.driver.find_element_by_id("focus")
        btn_send.click()
        self.driver.implicitly_wait(5)
        sleep(1)
        link_cancel = self.driver.find_element_by_id("cancel")
        link_cancel.click()
        self.driver.switch_to.window(current_tab)
        sleep(1)
        link_delete = self.driver.find_element_by_xpath("//span[@class='sA'][contains(text(),'eliminar')]")
        link_delete.click()
        self.driver.implicitly_wait(5)
        btn_ok = self.driver.find_element_by_name("ok")
        sleep(1)
        btn_ok.click()
        self.driver.implicitly_wait(5)
        alert = self.driver.find_element_by_class_name("bBe")
        sleep(1)
        alert.click()
        self.driver.implicitly_wait(5)



    def change_radio_buttons_values(self):
        sleep(2)
        div_to_move = self.driver.find_element_by_class_name("l2.pfiaof")
        self.action.move_to_element(div_to_move).perform()
        self.switch_to("bx_amard")
        self.switch_to("sender_attribution_setting")
        sleep(2)

    def open_buy_storage_link(self):
        link_buy_storage = self.driver.find_element_by_xpath("//a[contains(text(),'Comprar más almacenamiento')]")
        current_tab = self.driver.current_window_handle
        self.driver.implicitly_wait(5)
        link_buy_storage.click()
        self.window_handler()
        self.driver.implicitly_wait(5)
        btn_more_info = self.driver.find_element_by_xpath("//body//c-wiz//c-wiz//button//div[2]")
        btn_more_info.click()
        sleep(1)
        self.driver.implicitly_wait(5)
        btn_more_options = self.driver.find_element_by_xpath("//span[contains(text(),'Más opciones')]")
        btn_more_options.click()
        self.driver.implicitly_wait(5)
        btn_option = self.driver.find_element_by_xpath("//div[7]//div[1]//div[1]//button[1]")
        btn_option.click()
        self.driver.implicitly_wait(5)
        btn_cancel = self.driver.find_element_by_xpath("//span[contains(text(),'Cancelar')]")
        sleep(1)
        btn_cancel.click()
        self.driver.implicitly_wait(5)
        sleep(1)
        self.driver.close()
        sleep(1)
        self.driver.switch_to.window(current_tab)

    def into_gmail(self, email, password, previous_tab):
        self.page_inicial = PageInicial(self.driver)
        self.page_inicial.aux_email_input(email)
        self.page_inicial.btn_next()
        self.page_inicial.ingreso_password(password)
        btn_nextTwo = self.driver.find_element_by_class_name("RveJvd.snByac")
        assert btn_nextTwo.text == "Siguiente"
        btn_nextTwo.click()
        self.driver.implicitly_wait(5)
        sleep(3)
        self.driver.close()
        self.driver.switch_to.window(previous_tab)
        self.driver.refresh()
        sleep(2)
        account_logo = self.driver.find_element_by_class_name("gb_Ia.gbii")
        account_logo.click()
        txt_email = self.driver.find_element_by_xpath("//div[contains(text(),'"+email+"')]")
        assert txt_email.text == email
        account_logo.click

    def switch_to(self, value):
        for i in range(1, -1, -1):
            sleep(1)
            rbt_list = self.driver.find_elements_by_name(value)
            sleep(1)
            rbt_list[i].click()
            self.driver.implicitly_wait(5)
            alert = self.driver.find_element_by_class_name("bBe")
            sleep(1)
            alert.click()
            self.driver.implicitly_wait(5)

    def window_handler(self):
        current_tab = self.driver.current_window_handle
        for window_handle in self.driver.window_handles:
            if window_handle != current_tab:
                self.driver.switch_to.window(window_handle)
                break

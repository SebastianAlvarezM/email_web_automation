from time import sleep
from selenium.webdriver.support.ui import Select


class PageGeneralSettings:

    def __init__(self, test):
        self.driver = test

    def goToSettings(self):
        btn_configuracion = self.driver.find_element_by_class_name("T-I.J-J5-Ji.ash.T-I-ax7.L3")
        btn_configuracion.click()
        sleep(2)
        btn_irAConfiguracion = self.driver.find_element_by_xpath("//div[@id='ms']")
        btn_irAConfiguracion.click()
        self.driver.implicitly_wait(10)

    def selectSettingOption(self, option):
        which_option = self.driver.find_element_by_xpath("//a[contains(text(),'" + option + "')]")
        which_option.click()
        self.driver.implicitly_wait(5)

    def language(self, language):
        drop_language = Select(self.driver.find_element_by_class_name('a5p'))
        drop_language.select_by_visible_text(language) # select by visible text
        # dropdown_language.select_by_value('en-GB') #select by value
        drop_language.select_by_visible_text('Español')
        self.driver.implicitly_wait(5)

    def TelephoneNumberCodeCountry(self, country):
        drop_country = Select(self.driver.find_element_by_xpath("//td[contains(text(),'Código de país predeterminado:')]//select"))
        drop_country.select_by_visible_text(country)
        drop_country.select_by_visible_text('Colombia')
        self.driver.implicitly_wait(5)

    def ConversationsForPage(self, value):
        drop_ConversationsForPage = Select(self.driver.find_element_by_class_name('rA'))
        drop_ConversationsForPage.select_by_visible_text(value)
        self.driver.implicitly_wait(5)

    def ShippingCancellationPeriod(self, value):
        drop_ShippingCancellationPeriod = Select(self.driver.find_element_by_xpath("//div[@class='rc']//select"))
        drop_ShippingCancellationPeriod.select_by_visible_text(value)
        self.driver.implicitly_wait(5)

    def DefaultResponseForm(self, answer):

        if answer == 'Responder':
            radio_answer = self.driver.find_element_by_xpath("//input[@name='ix_dra' and @value='1']")
            radio_answer.click()
        else:
            radio_answerAll = self.driver.find_element_by_xpath("//input[@name='ix_dra' and @value='2']")
            radio_answerAll.click()
        self.driver.implicitly_wait(5)

    def ActionsOfPlacingTheCursorOnAnElement(self, actions):

        if actions == 'Habilitar acciones de colocar el cursor sobre un elemento':
            radio_actions1 = self.driver.find_element_by_xpath("//input[@name='bx_had' and @value='1']")
            radio_actions1.click()
        else:
            radio_actions2 = self.driver.find_element_by_xpath("//input[@name='bx_had' and @value='2']")
            radio_actions2.click()
        self.driver.implicitly_wait(5)

    def SendAndArchiveButton(self, font):
        list_font = self.driver.find_element_by_class_name('J-Z-axO J-Z-M-I aOz J-J5-Ji')
        list_font.click()
        opt_font = self.driver.find_element_by_id(font)
        opt_font.click()
        #list_fontSize = self.driver.find_element_by_class_name('J-Z-M-I J-J5-Ji')
        #list_fontSize.click()



    def DefaultTextStyle(self):
        pass

    def Images(self):
        pass

    def DynamicMail(self):
        pass

    def Autocorrect(self):
        pass

    def SmartWriting(self):
        pass

    def CustomizationSmartWriting(self):
        pass

    def ExperimentalAccess(self):
        pass

    def ConversationView(self):
        pass










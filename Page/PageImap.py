from time import sleep
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.firefox.webdriver import WebDriver


class PageImap:

    def __init__(self, Test):
        self.driver = Test
        self.wait = WebDriverWait(self.driver, 20)
        self.email2 = "emaildeprueba@gmail.com"

    def ingresoGmail(self):
        """ #No lo logré clieckando el botón de gapps
        btn_gapps = driver.find_element_by_class_name("gb_D.gb_xc")
        print("Se encontró el elemento")
        btn_gapps.click()
        print("Se clickeó el elemento")
        btn_gmail = wait.until(ec.presence_of_element_located((By.XPATH, '//*[@class="CgwTDb"]/*[@pid="23"]')))
        """
        btn_gmail = self.wait.until(ec.presence_of_element_located((By.XPATH, '//*[@class="gb_h gb_i"]/*[@data-pid="23"]')))
        btn_gmail.click()
        self.wait.until(ec.presence_of_element_located((By.CLASS_NAME, "T-I.J-J5-Ji.ash.T-I-ax7.L3")))

    def ingresoConfiguraciones(self):
        action = ActionChains(self.driver)
        btn_settings = self.driver.find_element_by_class_name("T-I.J-J5-Ji.ash.T-I-ax7.L3")
        btn_settings.click()
        action.move_to_element(btn_settings).perform()
        btn_config = self.driver.find_element_by_id('ms')
        print("Se encontró configuraciones")
        btn_config.click()
        self.wait.until(ec.presence_of_element_located((By.CLASS_NAME, "f0.LJOhwe")))

    def ingresoImap(self):
        # main_window = self.driver.window_handles[0]
        sleep(1)
        action = ActionChains(self.driver)
        btn_imap = self.driver.find_element_by_xpath(
            '//*[@href="https://mail.google.com/mail/u/0/?ogbl#settings/fwdandpop"]')  # ('//*[@class="nH fY"]/div[6]') or  #class_name("f0.LJOhwe")
        action.move_to_element(btn_imap).click().perform()
        print("Se encontró imap")
        btn_imap.click()

    def reenvio(self):
        link_info = self.wait.until(ec.presence_of_element_located(
            (By.XPATH, '//*[@href="https://support.google.com/mail/answer/10957?hl=es-419"]')))
        link_info.click()
        sleep(2)
        self.driver.switch_to.frame("google-feedback-wizard")
        btn_close_link_info = self.wait.until(
            ec.presence_of_element_located((By.CLASS_NAME, "ghp-header-closeIcon.ghp-closeIcon")))
        sleep(2)
        btn_close_link_info.click()
        self.driver.switch_to.default_content()

        # Formulario filtro
        link_filter = self.wait.until(
            ec.presence_of_element_located((By.XPATH, '//*[@class="r9"]/div/div[3]/span[@class="e"]')))
        link_filter.click()

        input_from = self.wait.until(ec.presence_of_element_located((By.CLASS_NAME, "ZH.nr.aQa")))
        input_from.send_keys("de@prueba.com")
        input_to = self.driver.find_element_by_class_name("ZH.nr.aQf")
        input_to.send_keys("para@prueba.com")
        input_subject = self.driver.find_element_by_class_name("ZH.nr.aQd")
        input_subject.send_keys("AsuntoPrueba")
        input_content = self.driver.find_element_by_class_name("ZH.nr.aQb")
        input_content.send_keys("Palabras Prueba")
        input_no_content = self.driver.find_element_by_class_name("ZH.nr.aP9")
        input_no_content.send_keys("Palabritas Pruebita")

        #input_to = self.driver.find_element_by_class_name("ZH.nr.aQb")
        #input_to.send_keys("para@prueba.com")

        drop_size = self.driver.find_element_by_xpath('//*[@class="aQi"]')
        drop_size.click()
        option_size_higher = self.driver.find_element_by_xpath('//*[@class="aQi"]/*[@class="J-M J-M-ayU"]/div[1]')
        option_size_higher.click()
        drop_size.click()
        option_size_lower = self.driver.find_element_by_xpath('//*[@class="aQi"]/*[@class="J-M J-M-ayU"]/div[2]')
        option_size_lower.click()

        input_size = self.driver.find_element_by_xpath('//*[@class="aQh"]/*[@class="nr"]')
        input_size.send_keys('101')

        drop_unit = self.driver.find_element_by_xpath('//*[@class="aQj"]')
        drop_unit.click()
        option_unit_mb = self.driver.find_element_by_xpath('//*[@class="aQj"]/*[@class="J-M J-M-ayU"]/div[1]')
        option_unit_mb.click()
        drop_unit.click()
        option_unit_kb = self.driver.find_element_by_xpath('//*[@class="aQj"]/*[@class="J-M J-M-ayU"]/div[2]')
        option_unit_kb.click()
        drop_unit.click()
        option_unit_bytes = self.driver.find_element_by_xpath('//*[@class="aQj"]/*[@class="J-M J-M-ayU"]/div[3]')
        option_unit_bytes.click()

        check_files = self.driver.find_element_by_xpath('//*[@class="w-Nw boo bs2"]/span[1]')
        check_files.click()
        check_chats = self.driver.find_element_by_xpath('//*[@class="w-Nw boo bs2"]/span[2]')
        check_chats.click()

        btn_filter = self.driver.find_element_by_class_name("acM")
        btn_filter.click()
        self.wait.until(ec.presence_of_element_located((By.XPATH, '//*[@class="btl lY"]/*[@class="nH"]/div[1]/label')))

        # btn_search = self.driver.find_element_by_class_name("T-I.J-J5-Ji.Zx.aQe.T-I-atl.L3")
        # btn_search.click()

        list_checkbox = self.driver.find_elements_by_class_name("aD")
        size_list_checkbox = len(list_checkbox)
        for x in range(2):
            for option in range(size_list_checkbox):
                list_checkbox[option].click()
            check_only_label = self.driver.find_element_by_xpath('//*[@class="btl ZQ"]/label')
            check_only_label.click()

        """# Otra manera de hacerlo:
        for x in range(2):
            for i in range(1, 11):
                if i != 5:
                    check_any = self.driver.find_element_by_xpath('//*[@class="btl lY"]/*[@class="nH"]/div[' + str(i) +
                                                                  ']/label')
                    check_any.click()

            check_only_label = self.driver.find_element_by_xpath('//*[@class="btl ZQ"]/label')
            check_only_label.click()
        """

        drop_tag = self.driver.find_element_by_class_name("T-axO.T-I.T-I-ax7.J-J5-Ji.J-JN-M-I.ZE")
        drop_tag.click()

        list_tags = self.driver.find_elements_by_xpath('//*[@class="J-N" and @role="option"]')
        tags_array = []

        for option in range(len(list_tags)):
            if list_tags[option].text[0:16] == 'EtiquetaDePrueba':
                text = list_tags[option].text
                tags_array.append(text)

        tag = 1
        for option in range(len(tags_array)):
            if tags_array[option] == 'EtiquetaDePrueba' + str(tag):
                tag += 1
            else:
                break

        option_tag_new = self.wait.until(
            ec.presence_of_element_located((By.XPATH, '//*[@class="J-M J-M-ayU aeE"]/div[2]')))
        option_tag_new.click()

        box_tag_new = self.wait.until(ec.presence_of_element_located((By.XPATH, '//input[@class="xx"]')))
        box_tag_new.send_keys('EtiquetaDePrueba' + str(tag))

        btn_create_tag = self.wait.until(ec.presence_of_element_located((By.CLASS_NAME, "J-at1-auR")))
        btn_create_tag.click()

        # btn_cancel = self.driver.find_element_by_name("cancel")
        # btn_cancel.click()

        # drop_tag.click()
        # option_tag = self.driver.find_element_by_xpath('//*[@class="T-axO T-I T-I-ax7 J-J5-Ji J-JN-M-I ZE"]/div[1]')
        # option_tag.click()

        drop_category = self.wait.until(ec.presence_of_element_located((By.XPATH, '//*[@class="T-axO T-I T-I-ax7 J-J5-Ji J-JN-M-I ZE IA"]')))

        #for x in range(2):
        for i in range(1, 6):
            if i != 5:
                drop_category.click()
                option_category = self.driver.find_element_by_xpath('//*[@class="J-M J-M-ayU"]/div[' + str(i) + ']')
                option_category.click()

        """
        option_principal = self.driver.find_element_by_xpath('//*[@class="J-M J-M-ayU"]/div[2]')
        option_principal.click()
        drop_category.click()
        option_social = self.driver.find_element_by_xpath('//*[@class="J-M J-M-ayU"]/div[3]')
        option_social.click()
        drop_category.click()
        option_notifications = self.driver.find_element_by_xpath('//*[@class="J-M J-M-ayU"]/div[4]')
        option_notifications.click()
        drop_category.click()
        option_forums = self.driver.find_element_by_xpath('//*[@class="J-M J-M-ayU"]/div[5]')
        option_forums.click()
        drop_category.click()
        option_deals = self.driver.find_element_by_xpath('//*[@class="J-M J-M-ayU"]/div[6]')
        option_deals.click()
        drop_category.click()
        option_choose = self.driver.find_element_by_xpath('//*[@class="J-M J-M-ayU"]/div[1]')
        option_choose.click()
        """

        btn_create_filter = self.driver.find_element_by_class_name("T-I.J-J5-Ji.Zx.acL.T-I-atl.L3")
        btn_create_filter.click()

        btn_sure = self.wait.until(ec.presence_of_element_located((By.CLASS_NAME, "J-at1-auR.J-at1-atl")))
        btn_sure.click()

        btn_add_email = self.wait.until(ec.presence_of_element_located((By.XPATH, '//*[@class="r9"]/div/div/input[@type="button"]')))
        btn_add_email.click()
        input_add_email = self.wait.until(ec.presence_of_element_located((By.XPATH, '//*[@class="PN"]/input')))
        input_add_email.send_keys(self.email2)
        btn_next = self.driver.find_element_by_name("next")
        btn_next.click()
        self.driver.switch_to.window(self.driver.window_handles[1])
        sleep(1)
        # print("El titulo de la URL secundaria es: %s" % self.driver.title)
        btn_continue = self.wait.until(ec.presence_of_element_located((By.XPATH, '//td/*[@type="submit"]')))
        btn_continue.click()
        sleep(1)
        self.driver.switch_to.window(self.driver.window_handles[0])
        btn_accept = self.driver.find_element_by_name("ok")
        btn_accept.click()

        # assert self.driver.find_element_by_xpath('//*[@id=":t0"]/table/tbody/tr[4]/td[1]/b').text == "Verificar {}.".format(self.email2) or "Verify {}.".format(self.email2)

        btn_remove_email = self.wait.until(ec.presence_of_element_located((By.XPATH, '//*[@act="removeAddr"]')))
        btn_remove_email.click()

        btn_ok = self.wait.until(ec.presence_of_element_located((By.NAME, 'ok')))
        btn_ok.click()

        sleep(2)

        alert_delete = self.wait.until(ec.presence_of_element_located((By.CLASS_NAME, 'bBe')))
        print("se encontró la alerta")
        alert_delete.click()

    def descarga(self):
        link_info = self.driver.find_element_by_xpath('//*[@href="https://support.google.com/mail/answer/10350?hl=es-419"]') #('//tr[2]/td[1]/a')
        link_info.click()
        self.driver.switch_to.window(self.driver.window_handles[1])
        sleep(1)
        self.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        sleep(1)
        #print("El titulo de la URL secundaria es: %s" % self.driver.title)
        self.driver.close()
        self.driver.switch_to.window(self.driver.window_handles[0])
        #print("El titulo de la URL principal es: %s" % self.driver.title)

        check_pop1 = self.wait.until(ec.presence_of_element_located((By.XPATH, "//table[1]/tbody/tr/td/input[@name='bx_pe']")))
        check_pop1.click()

        check_pop2 = self.driver.find_element_by_xpath("//table[2]/tbody/tr/td/input[@name='bx_pe']")
        check_pop2.click()

        drop_select = self.driver.find_element_by_xpath("//td[@class='r9']/div[2]/select[1]")

        for option in drop_select.find_elements_by_tag_name('option'):
            option.text == 'marcar la copia de Gmail como leída'
            option.click()
            option.text == 'archivar una copia de Gmail'
            option.click()
            option.text == 'eliminar la copia de Gmail'
            option.click()
            option.text == 'conservar la copia de Gmail en Recibidos'
            option.click()

        link_info = self.driver.find_element_by_xpath('//*[@href="https://support.google.com/mail/answer/12103?hl=es-419"]') #('//tr[2]/td[1]/a')
        link_info.click()
        self.driver.switch_to.window(self.driver.window_handles[1])
        sleep(1)
        self.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        sleep(1)
        #print("El titulo de la URL secundaria es: %s" % self.driver.title)
        self.driver.close()
        self.driver.switch_to.window(self.driver.window_handles[0])
        #print("El titulo de la URL principal es: %s" % self.driver.title)

    def acceso(self):
        link_info = self.wait.until(ec.presence_of_element_located((By.XPATH, '//*[@href="https://support.google.com/mail/answer/75725?hl=es-419"]')))
        link_info.click()
        self.driver.switch_to.window(self.driver.window_handles[1])
        sleep(1)
        self.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        sleep(1)
        self.driver.close()
        self.driver.switch_to.window(self.driver.window_handles[0])

        check_imap2 = self.wait.until(ec.presence_of_element_located((By.XPATH, "//table[2]/tbody/tr/td/input[@name='bx_ie']")))
        check_imap2.click()

        check_imap1 = self.driver.find_element_by_xpath("//table[1]/tbody/tr/td/input[@name='bx_ie']")
        check_imap1.click()

        check_imap_delete1 = self.wait.until(ec.presence_of_element_located((By.XPATH, "//table[1]/tbody/tr/td/input[@name='bx_iae']")))
        check_imap_delete1.click()

        check_imap_delete2 = self.driver.find_element_by_xpath("//table[2]/tbody/tr/td/input[@name='bx_iae']")
        check_imap_delete2.click()

        self.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")

        check_imap_delete3 = self.wait.until(ec.presence_of_element_located((By.XPATH, "//table[1]/tbody/tr/td/input[@name='ix_ieb']")))
        check_imap_delete3.click()

        check_imap_delete4 = self.driver.find_element_by_xpath("//table[2]/tbody/tr/td/input[@name='ix_ieb']")
        check_imap_delete4.click()

        check_imap_delete5 = self.driver.find_element_by_xpath("//table[3]/tbody/tr/td/input[@name='ix_ieb']")
        check_imap_delete5.click()

        check_imap_limit1 = self.wait.until(ec.presence_of_element_located((By.XPATH, "//table[1]/tbody/tr/td/input[@name='ix_ifm']")))
        check_imap_limit1.click()

        btn_warning = self.wait.until(ec.presence_of_element_located((By.CLASS_NAME, 'J-at1-auR')))
        btn_warning.click()

        check_imap_limit2 = self.wait.until(ec.presence_of_element_located((By.XPATH, "//table[2]/tbody/tr/td/input[@name='ix_ifm']")))
        check_imap_limit2.click()

        drop_limit = self.driver.find_element_by_xpath("//tr[@class='C7']/td/select[1]")

        for option in drop_limit.find_elements_by_tag_name('option'):
            option.text == '2,000'
            option.click()
            option.text == '5,000'
            option.click()
            option.text == '10,000'
            option.click()
            option.text == '1,000'
            option.click()

        link_info2 = self.driver.find_element_by_xpath('//*[@href="https://support.google.com/mail/answer/75726?hl=es-419"]')
        link_info2.click()
        self.driver.switch_to.window(self.driver.window_handles[1])
        sleep(1)
        self.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        sleep(1)
        self.driver.close()
        self.driver.switch_to.window(self.driver.window_handles[0])


        btn_cancel = self.driver.find_element_by_xpath("//div[@class = 'nH Tv1JD']/div[@class = 'nH r4']//button[@class = 'Gm']")
        btn_cancel.click()
        #btn_save_changes = self.driver.find_element_by_xpath('//*[@guidedhelpid="save_changes_button"]')
        #btn_save_changes.click()


from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions


class PageInicial:
    def __init__(self, Test):
        self.driver = Test

    def ingreso_dato(self, email):
        btn_start_session = self.driver.find_element_by_id('gb_70')
        assert btn_start_session.text == "Iniciar sesión"
        btn_start_session.click()
        self.driver.implicitly_wait(5)
        box_email = self.driver.find_element_by_id("identifierId")
        box_email.click()
        box_email.send_keys(email)
        self.driver.implicitly_wait(5)

    def btn_next(self):
        # submit = WebDriverWait(self.driver, 5).until(expected_conditions.element_to_be_clickable(self.btn_next()))
        btn_next = self.driver.find_element_by_class_name("RveJvd.snByac")
        btn_next.click()
        self.driver.implicitly_wait(5)

    def ingreso_password(self, password):
        box_password = self.driver.find_element_by_xpath("//*[@name='password']")
        box_password.click()
        box_password.send_keys(password)
        self.driver.implicitly_wait(5)

    def ver_password(self):
        show_password_close = self.driver.find_element_by_class_name('wRNPwe.pVlEsd')
        show_password_close.click()
        self.driver.implicitly_wait(5)
        show_password_open = self.driver.find_element_by_class_name('wRNPwe.S7pdP')
        show_password_open.click()
        self.driver.implicitly_wait(5)

    def ingreso_correo(self):
        btn_nextTwo = self.driver.find_element_by_class_name("RveJvd.snByac")
        btn_nextTwo.click()
        self.driver.implicitly_wait(5)
        link_gmail = self.driver.find_element_by_xpath("//a[text()='Gmail']")
        link_gmail.click()
        self.driver.implicitly_wait(5)

    def encontrar_avatar(self):
        btn_avatar = self.driver.find_element_by_class_name('gb_Ia.gbii') #Kathe
        btn_avatar.click()
        self.driver.implicitly_wait(5)

    def cerrar_sesion(self):
        btn_logout = self.driver.find_element_by_id('gb_71')
        btn_logout.click()

    def aux_email_input(self, email):
        box_email = self.driver.find_element_by_id("identifierId")
        box_email.click()
        box_email.send_keys(email)
        self.driver.implicitly_wait(5)

"""
General: Monica avendaño
Etiquetas: Isabel palmera
Recibidos: Diosymar
Cuentas e importación: Sebastian Guevara
Filtros y direcciones bloqueadas: Katherine 
Reenvío y correo POP/IMAP: Jorge Uribe
Complementos: Sebastian
Chat: Other

"""
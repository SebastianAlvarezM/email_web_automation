import configparser
from time import sleep
from selenium import webdriver
from Page.PageInicial import PageInicial


class PageLogIn:

    def __init__(self, Test):
        self.driver = Test

    def validatename(self):
        login_name_true = self.driver.find_element_by_class_name('gb_rb.gb_sb')
        if login_name_true.text == "Automation Test":
            print("Es el nombre correcto")
        login_email_true = self.driver.find_element_by_class_name('gb_tb')
        assert login_email_true.text == "automationwebqaco@gmail.com"
        print("Es el email correcto")
        self.driver.implicitly_wait(5)

    def intogmail(self):
        self.config = configparser.ConfigParser()
        self.config.read('keys.ini')
        self.page_inicial = PageInicial(self.driver)
        self.page_inicial.ingreso_dato(self.config['TEST_DATA']['email'])
        self.page_inicial.btn_next()
        self.page_inicial.ingreso_password(self.config['TEST_DATA']['password'])
        self.page_inicial.ingreso_correo()

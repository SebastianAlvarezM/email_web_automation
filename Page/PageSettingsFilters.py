

class PageSettingsFilters:
    def __init__(self, Test):
        self.driver = Test

    def goFilter(self):
        tab_filter = self.driver.find_element_by_class_name('f0 LJOhwe')
        tab_filter.click()

    def createFilter(self):
        link_createFilter = self.driver.find_element_by_id(':20')
        link_createFilter.click()
        email_filter = "mailer-daemon@googlemail.com"
        input_from = self.driver.find_element_by_id(':1u')
        input_from.send_keys(email_filter)
        btn_createFilter = self.driver.find_element_by_id(':4i')
        btn_createFilter.click()
        check_read = self.driver.find_element_by_class_name('aD')
        check_read.click()
        btn_createfilter2 = self.driver.find_element_by_id(':a4')
        btn_createfilter2.click()
        text_filter = self.driver.find_element_by_class_name('qV r5')
        assert text_filter.text == email_filter

    def removeFilter(self):
        check_selectFilter = self.driver.find_element_by_id(':lu')
        check_selectFilter.click()
        btn_remove = self.driver.find_element_by_id(':lo')
        btn_remove.click()

def tearDown(self):
    self.driver.close()


from time import sleep
from selenium import webdriver


class Pageinbox:

    def __init__(self, test):
        self.driver = test

    def btninboxconfig(self):
        btn_config = self.driver.find_element_by_class_name("T-I.J-J5-Ji.ash.T-I-ax7.L3")
        btn_config.click()
        self.driver.implicitly_wait(5)

    def btn_write_new_email(self):
        self.driver.implicitly_wait(5)
        btn_write = self.driver.find_element_by_xpath("//div[contains(text(),'Redactar')]")
        btn_write.click()
        self.driver.implicitly_wait(5)

    def btn_menu(self):
        btn_menu = self.driver.find_element_by_class_name("gb_zc")
        btn_menu.click()
        self.driver.implicitly_wait(5)

    def openGeneralSettingsPage(self):
        opt_config = self.driver.find_element_by_id('ms')
        opt_config.click()
        self.driver.implicitly_wait(5)



# webdriver.Chrome(executable_path="")
"""
Unit test: es una forma de comprobar que fragmento de condigo funciona correctamente. Es un procedimiento mas de los que se llevan a cabo
dentro de una metodologia agil de trabajo.

Podemos probrar una prueba o un conjunto de pruebas.

En todos los lenguajes de programacion se manejan las librerias de pruebas unitarias. En nuestro caso manejaremos la libreria de
python. La cual se llama Unittest esta libreria ya esta por defecto con la version que tenemos de python. Si usamos otra libreria
toca instalarla por medio del comando pip3.

Conceptos:

Test Case: Unidad minima en unittest puede contener una o mas validaciones. Esta hereda de unittest TestCase class

Test Fixture: Es la preparacion y terminacion de todas tus pruebas. Ejemplo: Abrir el navegador y
el otro ejemplo es terminar cerrando el navegador.

Test Suite: Una coleccion o grupo de casos de pruebas (Test Case)

Test Runner: La parte de unittest que ejecuta las pruebas

Test Report: Reporte de los resultados de las pruebas

Tomamos esta libreria para iniciar en el mundo de las pruebas automatizadas con selenium WebDriver, pero podemos usar mas librerias
como por ejemplo: Nose o Pytest (La cual vamos a usar la automatizacion de las app).

Una clase es un tipo de dato definido por el usuario, y al crear instancias de una clase hace relación
a la creación de objetos de ese tipo. Las clases y los objetos son considerados los principales bloques de desarrollo para Python.

Sintaxis

import unittest

class Nombre(herencia (unittest.TestCase)):
    Se crean las dos funciones del Test fixture.

    def setUp(selt): parámetro self se refiere al objeto instanciado de esa clase sobre el cual se está invocando dicho método
        global = driver

    las funciones de tus pruebas.
    def testNombre(self):


    def tearDown(selt):
        driver.close()


if __name__ ==" __ main __": (Es una forma estandar para asegurar que este unittest esta corriendo como programa independiente
                                y no es llamado de otro mudulo. Si se fuera a llamar en otro modulo no ponerlo.) Alinado con la class
                                tener cuidado con las minusculas y las mayusculas.
unittest.main()


"""
import unittest
from selenium import webdriver
from time import sleep


class loginCorreo(unittest.TestCase):

    def setUp(self):
        global driver
        driver = webdriver.Firefox(executable_path='/Users/Tatay/Browsers/geckodriver')
        driver.get('http://www.google.com.co')
        sleep(2)

    def testingresoEmail(self):
        email = "automationwebqaco@gmail.com"
        btn_start_session = driver.find_element_by_id('gb_70')
        assert btn_start_session.text == "Iniciar sesión"
        print("se encontro el elemento")
        btn_start_session.click()
        sleep(2)
        box_email = driver.find_element_by_id("identifierId")
        box_email.click()
        box_email.send_keys(email)
        print("Se ingreso el correo {}.".format(email))
        sleep(2)
        btn_next = driver.find_element_by_class_name("RveJvd.snByac")
        assert btn_next.text == "Siguiente"
        print("Se encontro el boton se siguiente")
        btn_next.click()
        sleep(2)
        box_password = driver.find_element_by_xpath("//*[@name='password']")
        if box_password is not None:
            print("Se encontro el campo de contraseña")
        box_password.click()
        box_password.send_keys('Web12345Auto')
        sleep(2)
        show_password_close = driver.find_element_by_class_name('wRNPwe.pVlEsd')
        show_password_close.click()
        sleep(2)
        show_password_open = driver.find_element_by_class_name('wRNPwe.S7pdP')
        show_password_open.click()
        sleep(2)
        btn_nextTwo = driver.find_element_by_class_name("RveJvd.snByac")
        assert btn_nextTwo.text == "Siguiente"
        print("Se encontro el boton de siguiente")
        btn_nextTwo.click()
        sleep(2)
        btn_avatar = driver.find_element_by_class_name('gb_Ia.gbii')
        if btn_avatar is not None:
            print("Se encontro el logo de avatar")
        btn_avatar.click()
        sleep(2)
        login_name_true = driver.find_element_by_class_name('gb_rb.gb_sb')
        if login_name_true.text == "Automation Test":
            print("Es el nombre correcto")
        login_email_true = driver.find_element_by_class_name('gb_tb')
        assert login_email_true.text == "automationwebqaco@gmail.com"
        print("Es el email correcto")
        sleep(2)
        btn_logout = driver.find_element_by_id('gb_71')
        if btn_logout.text == "Salir":
            print("Se salio del correo y hemos terminado la prueba.")
            btn_logout.click()

    def tearDown(self):
        driver.close()


unittest.main()

"""email: AutomationWebQAco@gmail.com  clave: Web12345Auto"""

"""
Tarea para eñ fin de semana:
Udemy
https://www.udemy.com/
1- Login,
2- ingresar a perfil, confirmar ingreso a perfil.
3- Cambiar informacion basica y guardar.
4- navegar por todos los modulos
5- hacer logout (Si logra hacerlo explciar como lo hizo.)

"""